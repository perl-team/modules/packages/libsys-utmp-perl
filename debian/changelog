libsys-utmp-perl (1.8-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:23:09 +0100

libsys-utmp-perl (1.8-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update path in lintian override.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Florian Schlichting ]
  * Import upstream version 1.8
  * Add upstream metadata
  * Update copyright years
  * Mark package autopkgtest-able
  * Declare compliance with Debian Policy 4.1.1

 -- Florian Schlichting <fsfs@debian.org>  Sun, 08 Oct 2017 21:03:55 +0200

libsys-utmp-perl (1.7-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Use dist-based URL in debian/watch.
  * Split fix for the test suite out into empty_utmp.patch.
  * Activate additional tests by adding build dependencies on
    libtest-pod-perl, libtest-pod-coverage-perl.
  * Remove build dependency on historic dpkg-dev version.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 1.7
  * Switch to source format 3.0 (quilt)
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Switch to short-form debian/rules
  * Convert debian/copyright to machine-readable copyright-format 1.0
  * Add myself to uploaders
  * Declare compliance with Debian Policy 3.9.5
  * Add lintian override regarding missing hardening flags

 -- Florian Schlichting <fsfs@debian.org>  Tue, 29 Oct 2013 23:05:51 +0100

libsys-utmp-perl (1.6-4) unstable; urgency=low

  * Now maintained by the Debian perl group.

 -- Joey Hess <joeyh@debian.org>  Mon, 30 Jul 2007 01:22:56 -0400

libsys-utmp-perl (1.6-3) unstable; urgency=low

  * Fix the test suite to not die if the utmp file is empty.
    Closes: #394618

 -- Joey Hess <joeyh@debian.org>  Tue, 24 Oct 2006 12:31:48 -0400

libsys-utmp-perl (1.6-2) unstable; urgency=low

  * The test suite depends on the system having a utmp file, and since utmp
    files are IIRC not portable across arches, it's not easy to include a demo
    one for the tests to run against. Instead, if the utmp file is not
    present, skip the test suite. Closes: #394618

 -- Joey Hess <joeyh@debian.org>  Sun, 22 Oct 2006 15:16:41 -0400

libsys-utmp-perl (1.6-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Sun, 22 Oct 2006 00:53:24 -0400

libsys-utmp-perl (1.5-4) unstable; urgency=low

  * Update to current stadands-version.

 -- Joey Hess <joeyh@debian.org>  Sat, 23 Jul 2005 19:34:31 -0400

libsys-utmp-perl (1.5-3) unstable; urgency=low

  * Built for perl 5.8.

 -- Joey Hess <joeyh@debian.org>  Wed, 31 Jul 2002 02:42:19 +0000

libsys-utmp-perl (1.5-2) unstable; urgency=low

  * Debhelper v4. Fixed dpkg-dev build-dep.

 -- Joey Hess <joeyh@debian.org>  Mon, 29 Jul 2002 19:05:43 +0000

libsys-utmp-perl (1.5-1) unstable; urgency=low

  * Initial Release.

 -- Joey Hess <joeyh@debian.org>  Tue, 12 Feb 2002 20:49:36 -0500
